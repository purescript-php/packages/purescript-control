<?php

$exports["arrayExtend"] = function($f) {
  return function($xs) use (&$f) {
    $result = [];
    foreach ($xs as $i => $val) {
      $result[] = $f(array_slice($xs, $i));
    }
    return $result;
  };
};